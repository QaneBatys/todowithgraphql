const express = require("express");
const makeStoppable = require("stoppable");
const http = require("http");
const path = require("path");
const cookieSession = require("cookie-session");
const bodyParser = require("body-parser");
const { error } = require("console");

const { graphqlHTTP } = require('express-graphql')

const app = express();

const server = makeStoppable(http.createServer(app));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(
  cookieSession({
    name: "session",
    keys: ["Ghdur687399s7w", "hhjjdf89s866799"],
  })
);

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "./views"));

app.use(express.static(path.join(__dirname, "./static")));

const routes = require("./routes");
const schema  = require("./data/schema");

module.exports = () => {
  app.use("/", routes());
  app.use('/graphql',graphqlHTTP({
    schema:schema,
    graphiql:true
  }))

  const stopServer = () => {
    return new Promise((resolve) => {
      server.stop(resolve);
    });
  };

  return new Promise((resolve) => {
    server.listen(8080, () => {
      console.log("Express server is listening on http://localhost:3000");
      resolve(stopServer);
    });
  });
};
