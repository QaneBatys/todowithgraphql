const Task = require ("./task");

module.exports = {
    Query: {
        getTodoTasks: async () => {
            try{
                return await Task.find({done: false});
            }catch(err){
                throw new Error(err)
            };
        },
        getDoneTasks: async () => {
            try{
                return await Task.find({done: true});
            }catch(err){
                throw new Error(err)
            };
        },
        getTask: async (root, {id}) => {
            try{
                if(id) console.log('loh')
                return await Task.findById(id);
            }catch(err){
                throw new Error(err)
            };
        },
        findTasks: async (root, {tags}) => {
            try{
                return await Task.find({tags: {$all: tags}});
            }catch(err){
                throw new Error(err)
            };
        }
    },
    Mutation: {
        createTask: (root, { task }) => {
            const newTask = new Task({
                title: task.title,
                description: task.description,
                tags: task.tags,
                done: task.done || false
            });
        
            return newTask.save() // No callback function needed
                .then(savedTask => savedTask) // Return the saved task
                .catch(err => {
                    throw new Error(err); // Handle any errors
                });
        },
        updateTask: async (root, {id, task}) => {  
            try {
                const updatedTask = await Task.findByIdAndUpdate(id, task, { new: true });
                return updatedTask;
            } catch (error) {
                throw new Error(error);
            }
        },
        deleteTask: async (root, {id}) => {
            try{
                await Task.deleteOne({_id:id});
                return true;
            }catch(err){
                throw new Error(err)
            };
        }
    }
};