const mongoose = require('mongoose');

mongoose.Promise = global.Promise; 
mongoose.connect('mongodb://localhost:37017/tasks');

const TaskSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    tags: {
        type: [String],
        required: true
    },
    done: {
        type: Boolean,
        required: true,
        default: false // Assuming that new tasks are not done by default
    }
})

const Tasks = mongoose.model("tasks", TaskSchema);

module.exports = Tasks;