const express = require("express");
const router = express.Router();

module.exports = (params) => {
  const { tasksService } = params;

  router.get("/", async (req, res) => {
    res.render("layout", {
      pageTitle: "Done",
      template: "done",
      tasks
    });
  });

  //router.get("/api/:taskName",)


  

  return router;
};
